package Test_Classes;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import POM_Login.KiteZerodhaLoginPageDetails;
import io.github.bonigarcia.wdm.WebDriverManager;


public class TC2_verifyLoginPage   
{            
	WebDriver driver;
	KiteZerodhaLoginPageDetails loginPage;
	@BeforeTest
	public void launchBrowser()
	{
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		driver=new ChromeDriver(options);
		driver.navigate().to("https://kite.zerodha.com/");
	}
	@BeforeClass
	public void createObjects ()
	{
		loginPage = new KiteZerodhaLoginPageDetails(driver);
	}
	@BeforeMethod
	public void openUrl()
	{
		driver.navigate().to("https://kite.zerodha.com/");
		driver.manage().window().maximize();
		driver.navigate().refresh();
	}

	@Test
	public void loginPageVerify() throws InterruptedException
	{
		boolean checkImage = loginPage.checkKiteImages();
		Assert.assertEquals(checkImage, true);
		System.out.println("kite Image is available on webpage");
		Thread.sleep(3000);
		
		checkImage= loginPage.checkGooglePlayStoreImage();
		Assert.assertEquals(checkImage, true);
		System.out.println("Google Play Store Image is available on webpage");
		Thread.sleep(3000);
		
		checkImage= loginPage.checkApplePlayStoreImage();
		Assert.assertEquals(checkImage, true);
		System.out.println("Apple Play Store Image is available on webpage");
		Thread.sleep(3000);
		
		String kiteText = (String) loginPage.getLoginToKiteText();
		Assert.assertEquals(kiteText, "Login to Kite");
		System.out.println("Text present is correct");
		Thread.sleep(3000);
		
		String text = loginPage.clickNewAccLink();
		Assert.assertEquals(text, "Don't have an account? Signup now!");
		System.out.println("Text present is correct");
		Thread.sleep(3000);
		
		boolean versionText= loginPage.getAppVersionText();
		Assert.assertEquals(versionText, true);
		System.out.println("App. version is available on webpage");
		Thread.sleep(3000);
		
		loginPage.clickOnZerodhaImage();
		String actualUrl=driver.getCurrentUrl();
		Assert.assertEquals(actualUrl, "https://zerodha.com/");
		System.out.println("URL is correct");
		Thread.sleep(3000);
	}
	@AfterMethod
	public void refreshPageAgain()
	{
		driver.navigate().refresh();
	}
	@AfterClass
	public void clearObjectsr()
	{
		loginPage=null;
	
	}
	@AfterTest
	public void closeBrowser()
	{
		driver.quit();
		driver=null;
		System.gc();
	}

}
