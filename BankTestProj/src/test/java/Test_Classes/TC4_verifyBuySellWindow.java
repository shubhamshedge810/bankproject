package Test_Classes;

import java.io.File;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import POM_Login.KiteZerodhaHomePage;
import POM_Login.KiteZerodhaLoginPage;
import POM_Login.KiteZerodhaPinPage;
import POM_Login.kiteZerodhaBuySellWindow;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TC4_verifyBuySellWindow 
{
	WebDriver driver;
	KiteZerodhaLoginPage loginPage;
	KiteZerodhaPinPage pinPage;
	KiteZerodhaHomePage homePage;
	kiteZerodhaBuySellWindow buySellWindow;
	@BeforeTest
	public void launchbrowser()
	{
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		driver=new ChromeDriver(options);	}
	
	@BeforeClass
	public void createOject()
	{
		driver= new ChromeDriver();
		loginPage= new KiteZerodhaLoginPage(driver);
		pinPage= new KiteZerodhaPinPage(driver);
		homePage= new KiteZerodhaHomePage(driver);
		buySellWindow= new kiteZerodhaBuySellWindow (driver);
	}
	@BeforeMethod
	public void loginToPage() throws InterruptedException
	{
		driver.navigate().to("https://kite.zerodha.com/");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		loginPage.enterUserID("UAB157");
		loginPage.enterPassword("PLT9tv*229");
		loginPage.clickOnLoginButton();
		Thread.sleep(3000);
		
		pinPage.enterPin("255555");
		pinPage.clickOnContinueButton();
		Thread.sleep(3000);
		
		homePage.enterIntoSearch("SBIN");
		Thread.sleep(1000);
		homePage.clickOnShare();
		Thread.sleep(1000);
		homePage.clickOnBlueBuyButton();
	}
	@Test
	public void checkText()
	{
		
		ArrayList<String> list=buySellWindow.checkTest();
		for(int a=0; a<list.size(); a++)
		{
			System.out.println(list.get(a));
			
		}
	}
	@Test
	public void checkInputField()
	{
		
	}
	@AfterMethod
	public void logout() throws InterruptedException
	{

		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='user-id']")));
		
		homePage.ClickOnUserIdButton();
		Thread.sleep(1000);
		homePage.ClickOnLogoutButton();
		Thread.sleep(1000);
		loginPage.clickOnChangeUser();
	}
	@AfterClass
	public void clearObjects()
	{
		pinPage = null;
		loginPage = null;
		homePage = null;
		
	}
	@AfterTest
	public void closeBrowser()
	{
		driver.quit();
		driver= null;
		System.gc();
	}
	
}
