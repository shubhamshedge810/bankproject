package Test_Classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import POM_Login.KiteZerodhaHomePage;
import POM_Login.KiteZerodhaLoginPage;
import POM_Login.KiteZerodhaPinPage;
import POM_Login.PracticePOM;
import Test_Browser_Setup.shareSelection;




public class Practice_class extends shareSelection
{

	//private static final String WebElement = null;
	WebDriver driver;
//	@Test
//	public void dashboard() throws InterruptedException
//	{
//		System.setProperty("webdriver.chrome.driver", "D:\\CLASS_NST\\Automation\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.navigate().to("https://kite.zerodha.com/");
////		
////		System.setProperty("webdriver.gecko.driver", "D:\\CLASS_NST\\Automation\\geckodriver.exe");
////		driver = new FirefoxDriver();
////		driver.navigate().to("https://kite.zerodha.com/");
//		
//		driver.manage().window().maximize();
//		driver.navigate().refresh();
//		KiteZerodhaLoginPage login = new KiteZerodhaLoginPage(driver);
//		login.enterUserID("UQ0243");
//		login.enterPassword("Nik@0567");
//		login.clickOnLoginButton();
//		Thread.sleep(3000);
//		
//		KiteZerodhaPinPage pin = new KiteZerodhaPinPage(driver);
//		pin.enterPin("000567");
//		pin.clickOnContinueButton();
//		Thread.sleep(3000);
//		
//		KiteZerodhaHomePage select = new KiteZerodhaHomePage(driver);
//		select.enterIntoSearch("RELIANCE");
//		
////		shareSelectionCode selectOption = new shareSelectionCode();
////		selectOption.selectBuyButton(driver);-->> not working
//		
////		WebElement search = driver.findElement(By.xpath("(//div//input)"));
////		search.sendKeys("SBIN");
////		Thread.sleep(5000);
//		
//		
//		WebElement searchName = driver.findElement(By.xpath("//div[2]//ul//div//li[1]"));
//		Thread.sleep(5000);
//		
//		Actions act = new Actions(driver);
//		act.moveToElement(searchName).perform();
//		Thread.sleep(5000);
//		
//		WebElement bButton = driver.findElement(By.xpath("(//button[@class='button-blue'])[1]"));
//		bButton.click();
//		Thread.sleep(5000);
//						
//		
////		Thread.sleep(5000);
////		WebElement dropdown = driver.findElement(By.xpath("//div//ul//div//li[1]"));
////		dropdown.click();
////		Thread.sleep(5000);
//		
//		WebElement bseButton = driver.findElement(By.xpath("(//DIV//LABEL)[2]"));
//		bseButton.click();
//		Thread.sleep(3000);
//		
//		WebElement intradayButton = driver.findElement(By.xpath("(//DIV//LABEL)[6]"));
//		intradayButton.click();
//		Thread.sleep(3000);
//		
//		WebElement qtyFiled = driver.findElement(By.xpath("(//input[@autocorrect=\"off\"])[1]"));
//		qtyFiled.sendKeys("500");
//		Thread.sleep(3000);
//		
//		WebElement buyButton = driver.findElement(By.xpath("//button[@type='submit']"));
//		buyButton.click();
//		Thread.sleep(3000);
//		
//		
//		
//	}
	@Test
	public void loginverification() throws IOException, InterruptedException //paramterization try
	{
		System.setProperty("webdriver.chrome.driver", "D:\\CLASS_NST\\Automation\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("https://kite.zerodha.com/");
		
		driver.manage().window().maximize();
		driver.navigate().refresh();
		KiteZerodhaLoginPage login = new KiteZerodhaLoginPage(driver);
		
		File excelsheet = new File("C:\\Users\\91997\\Desktop\\SQL.xlsx");
		FileInputStream input = new FileInputStream(excelsheet);
		//String d = WorkbookFactory.create(input).getSheet("Sheet2").getRow(0).getCell(0).getStringCellValue();
		
		Sheet s = WorkbookFactory.create(input).getSheet("Sheet2");
		int rowCount = s.getLastRowNum();
		System.out.println(rowCount);
		
		Row row = s.getRow(0);
		int cellCount= row.getLastCellNum();
		System.out.println(cellCount);
		int k;
		for(int i=1; i<=(rowCount); i++)
		{					//0,1,2
			for(int j=0; j<(cellCount); j++)
			{
				try
				{
					String username= s.getRow(i).getCell(j).getStringCellValue();
					login.enterUserID(username);
					System.out.println(username);
					Thread.sleep(3000);
					k=++j;
					String pwd= s.getRow(i).getCell(k).getStringCellValue();
					login.enterPassword(pwd);
					System.out.println(pwd);
					Thread.sleep(3000);
					login.clickOnLoginButton();
					Thread.sleep(3000);
					WebElement e = driver.findElement(By.xpath("(//div//p)[1]"));
					boolean r=e.isDisplayed();
					if(r==true)
					{
						login.clearfield();
						//driver.navigate().refresh();	
					}
					else
					{
						KiteZerodhaPinPage pin = new KiteZerodhaPinPage(driver);
						pin.enterPin("000567");
						pin.clickOnContinueButton();
						Thread.sleep(3000);
					}
					//driver.navigate().refresh();
					
				}
				catch(IllegalStateException x)
				{
					int username = (int) s.getRow(i).getCell(j).getNumericCellValue();
					login.enterUserID(username);
					System.out.println(username);
					Thread.sleep(3000);
					k=++j;
					int pwd= (int) s.getRow(i).getCell(k).getNumericCellValue();
					login.enterPassword(pwd);
					System.out.println(pwd);
					Thread.sleep(3000);
					login.clickOnLoginButton();
					Thread.sleep(3000);
					WebElement e = driver.findElement(By.xpath("(//div//p)[1]"));
					boolean r=e.isDisplayed();
					if(r==true)
					{
						login.clearfield();
						//driver.navigate().refresh();	
					}
					else
					{
						KiteZerodhaPinPage pin = new KiteZerodhaPinPage(driver);
						pin.enterPin("000567");
						pin.clickOnContinueButton();
						Thread.sleep(3000);
					}
					//driver.navigate().refresh();
					
				}
				
			}
			System.out.println();
			
		}
		KiteZerodhaPinPage pin = new KiteZerodhaPinPage(driver);
		pin.enterPin("000567");
		Thread.sleep(1000);
		pin.clickOnContinueButton();
		
	}
	
//	@Test
//	public void  homepage() throws InterruptedException //method overloading try
//	{
//		System.setProperty("webdriver.chrome.driver", "D:\\CLASS_NST\\Automation\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.navigate().to("https://kite.zerodha.com/");
//		
//		KiteZerodhaLoginPage login = new KiteZerodhaLoginPage(driver);
//		login.enterUserID("UQ0243");
//		login.enterPassword("Nik@0567");
//		login.clickOnLoginButton();
//		Thread.sleep(3000);
//		
//		KiteZerodhaPinPage pin = new KiteZerodhaPinPage(driver);
//		pin.enterPin("000567");
//		pin.clickOnContinueButton();
//		Thread.sleep(3000);
//
//		PracticePOM pom = new PracticePOM(driver);
//		pom.click("Dashboard");
//		System.out.println("Dashboard");
//		pom.click(0);
//		System.out.println("Orders");
//		pom.click('P');
//		System.out.println("Positions");
//		pom.click(true);
//		System.out.println("Holdings");
//		pom.click(10);
//		System.out.println("Apps");
//		pom.click(0.0f);
//		System.out.println("Funds");
//	}	

}
