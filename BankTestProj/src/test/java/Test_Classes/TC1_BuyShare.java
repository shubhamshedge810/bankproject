package Test_Classes;
import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/*HERE WE WILL SEE THE TEST CLASS FOR POM CLASSES
 * 
 */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import POM_Login.KiteZerodhaHomePage;
import POM_Login.KiteZerodhaLoginPage;
import POM_Login.KiteZerodhaPinPage;
import POM_Login.PracticePOM;
import Test_Browser_Setup.Pojo;
import Utilities.Utility;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TC1_BuyShare extends Pojo
{
	WebDriver driver;
	KiteZerodhaPinPage pinPage;
	KiteZerodhaLoginPage loginPage;
	KiteZerodhaHomePage homePage;
	ChromeOptions options;
	
	@BeforeTest
	public void launchBrowser(){
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		driver=new ChromeDriver(options);
		options.addArguments("--disable-notifications");

	
	
	}
//	@Parameters("browser")
//	public void launchBrowser(@Optional("Abc") String browser) 
//	{
//		if(browser.equalsIgnoreCase("chrome"))
//		{
//			WebDriverManager.chromedriver().setup();
//			ChromeOptions options = new ChromeOptions();
//			driver=new ChromeDriver(options);
//			
//		}
//		else if (browser.equalsIgnoreCase("firefox"))
//		{
//			WebDriverManager.firefoxdriver().setup();
//			FirefoxOptions options = new FirefoxOptions();
//			driver=new FirefoxDriver(options);		}
//	}
//	public void launchBrowser()
//	{
//		System.setProperty("webdriver.chrome.driver", "D:\\CLASS_NST\\Automation\\Chrome-87\\chromedriver_87.exe");
//		driver = new ChromeDriver();
//	}
//	public void launchBrowser()
//	{
//		System.setProperty("webdriver.gecko.driver", "D:\\CLASS_NST\\Automation\\geckodriver.exe");
//		driver = new FirefoxDriver();
//	}
	@BeforeClass
	public void createObject()
	{
		pinPage = new KiteZerodhaPinPage(driver);
		loginPage = new KiteZerodhaLoginPage(driver);
		homePage = new KiteZerodhaHomePage(driver);
	}
	@BeforeMethod
	public void loginToPage() throws InterruptedException
	{
		driver.navigate().to("https://kite.zerodha.com/");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		loginPage.enterUserID("UAB157");
		loginPage.enterPassword("PLT9tv*229");
		loginPage.clickOnLoginButton();
		Thread.sleep(3000);
		
		pinPage.enterPin("255555");
		pinPage.clickOnContinueButton();
		Thread.sleep(3000);

	}
	
	@Test (priority=1)
	public void buyShare() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS );
		homePage.enterIntoSearch("SBIN");
		//Thread.sleep(1000);
		homePage.clickOnShare();
		//Thread.sleep(1000);
		homePage.clickOnBlueBuyButton();
		//Thread.sleep(1000);
		homePage.selectBSEButton();
		homePage.selectIntradayButton();
		homePage.enterQty("500");
		homePage.ClickOnBuyButton();
		
	}
	@Test(priority=2)
	public void checkHomepageOptions()
	{
		PracticePOM pom = new PracticePOM(driver);
		pom.click("Dashboard");
		System.out.println("Dashboard");
		pom.click(0);
		System.out.println("Orders");
		pom.click('P');
		System.out.println("Positions");
		pom.click(true);
		System.out.println("Holdings");
		pom.click(10);
		System.out.println("Apps");
		pom.click(0.0f);
		System.out.println("Funds");
		

	}

	@AfterMethod
	public void logOut(ITestResult result) throws InterruptedException, IOException
	{
		if(ITestResult.FAILURE == result.getStatus())
		{
			Utility.takeScreenshot(driver);
		}
		Thread.sleep(5000);
		homePage.ClickOnUserIdButton();
		Thread.sleep(1000);
		homePage.ClickOnLogoutButton();
		Thread.sleep(1000);
		loginPage.clickOnChangeUser();
		Thread.sleep(1000);
	}
	@AfterClass
	public void clearObjects()
	{
		pinPage = null;
		loginPage = null;
		homePage = null;
		
	}
	@AfterTest
	public void closeBrowser()
	{
		driver.quit();
		driver= null;
		System.gc();
	}
	
}
