package Test_Classes;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import POM_Login.KiteZerodhaHomePage;
import POM_Login.KiteZerodhaHomePageOptions;
import POM_Login.KiteZerodhaLoginPage;
import POM_Login.KiteZerodhaPinPage;
import Test_Browser_Setup.Pojo;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TC3_VerifyHomePageOptions //extends Pojo
{
	WebDriver driver;
	KiteZerodhaHomePageOptions options;
	KiteZerodhaPinPage pinPage;
	KiteZerodhaLoginPage loginPage;
	KiteZerodhaHomePage homePage;
	@BeforeTest
	public void launchBrowser()
	{
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		driver=new ChromeDriver(options);
	}

	@BeforeClass
	public void createObjects()
	{
		options = new KiteZerodhaHomePageOptions(driver);
		pinPage = new KiteZerodhaPinPage(driver);
		loginPage = new KiteZerodhaLoginPage(driver);
		homePage = new KiteZerodhaHomePage(driver);
	}
	
	@BeforeMethod
	public void loginToPage() throws InterruptedException
	{
		driver.navigate().to("https://kite.zerodha.com/");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		loginPage.enterUserID("RJ1522");
		loginPage.enterPassword("Wagholi@412207");
		loginPage.clickOnLoginButton();
		Thread.sleep(3000);
		
		pinPage.enterPin("231183");
		pinPage.clickOnContinueButton();
		Thread.sleep(3000);
	}
	
	@Test (priority=0)
	public void checkDashboard()
	{
		String dashboardURL=options.clickOnDashboard();
		Assert.assertEquals(dashboardURL, "https://kite.zerodha.com/dashboard");
		System.out.println("Dashboard URL is correct");
	}
	@Test (priority=1)
	public void checkOrders()
	{
		String ordersURL=options.clickOnOrders();
		Assert.assertEquals(ordersURL, "https://kite.zerodha.com/orders");
		System.out.println("Orders URL is correct");
	}
	@Test (priority=2)
	public void checkHoldings()
	{
		String holdingsURL=options.clickOnHoldings();
		Assert.assertEquals(holdingsURL, "https://kite.zerodha.com/holdings");
		System.out.println("Holdings URL is correct");
	}
	@Test (priority=3)
	public void checkPositions()
	{
		String positionsURL=options.clickOnPositions();
		Assert.assertEquals(positionsURL, "https://kite.zerodha.com/positions");
		System.out.println("Positions URL is correct");
	}
	@Test (priority=4)
	public void checkFunds()
	{
		String fundsURL=options.clickOnFunds();
		Assert.assertEquals(fundsURL, "https://kite.zerodha.com/funds");
		System.out.println("Funds URL is correct");
	}
	@Test (priority=5)
	public void checkApps()
	{
		String appsURL=options.clickOnApps();
		Assert.assertEquals(appsURL, "https://kite.zerodha.com/apps");
		System.out.println("Apps URL is correct");
	}
//	@Test (priority=0) // with method overloading
//	public void dashboardPage() throws InterruptedException
//	{
//		Thread.sleep(5000);
//		homePage.enterIntoSearch("SBIN");
//		homePage.clickOnShare();
//		Thread.sleep(3000);
//		homePage.clickOnBlueBuyButton();
//		Thread.sleep(3000);
//		homePage.selectBSEButton();
//		Thread.sleep(3000);
//		homePage.selectIntradayButton();
//		Thread.sleep(3000);
//		homePage.enterQty("500");
//		Thread.sleep(3000);
//		homePage.enterprice("190");
//		Thread.sleep(3000);
//		homePage.ClickOnBuyButton();
//		Thread.sleep(8000);
//	}
	@AfterMethod
	public void logOut() throws InterruptedException
	{
		homePage.ClickOnUserIdButton();
		Thread.sleep(1000);
		homePage.ClickOnLogoutButton();
		Thread.sleep(1000);
		loginPage.clickOnChangeUser();
		Thread.sleep(1000);
	}
	@AfterClass
	public void clearObjects()
	{
		pinPage = null;
		loginPage = null;
		homePage = null;
		
	}
	@AfterTest
	public void closeBrowser()
	{
		driver.quit();
		driver= null;
		System.gc();
	}

}
