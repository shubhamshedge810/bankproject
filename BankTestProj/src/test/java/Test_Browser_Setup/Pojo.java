package Test_Browser_Setup;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Pojo
{
	public static WebDriver openChromeDriver()
	{
		System.setProperty("webdriver.chrome.driver", "src"+File.separator+"main"+File.separator+"resources"+File.separator+"Browsers"+File.separator+"chromedriver_87.exe");
		WebDriver driver = new ChromeDriver();
		return driver;
	}
	
	public static WebDriver openFirefoxDriver()
	{
		System.setProperty("webdriver.gecko.driver", "src"+ File.separator +"main"+ File.separator+"resources"+File.separator+"Browsers"+File.separator+"geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		return driver;
	}

}
