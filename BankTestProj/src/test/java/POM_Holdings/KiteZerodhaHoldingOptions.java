package POM_Holdings;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class KiteZerodhaHoldingOptions {
  
	
	
	@FindBy (xpath="/html/body/div[1]/div[1]/div/div[2]/div[1]/a[3]/span")
	private WebElement holdingButton;

	public KiteZerodhaHoldingOptions(WebDriver driver) {
		
		PageFactory.initElements(driver, this);

		
	}
	
	public void clickOnHoldingBtn() {

		boolean result= holdingButton.isDisplayed();
		 
		 System.out.println(result);
		 
		 Assert.assertEquals(result, true);
		
	}
	
	
	
	
}
