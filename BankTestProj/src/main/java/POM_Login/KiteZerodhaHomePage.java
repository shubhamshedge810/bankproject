package POM_Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class KiteZerodhaHomePage 
{
	WebDriver driver;
	@FindBy (xpath="(//div//input)")
	private WebElement searchButton;
	
//	@FindBy (xpath="//div//ul//div//li[1]") only work for chrome browser
//	private WebElement selectShare;
	
	@FindBy (xpath="//div[2]//ul//div//li[1]") 
	private WebElement selectShare;
	
	@FindBy (xpath="(//button[@class='button-blue'])[1]") 
	private WebElement selectBlueBuyButton;
	
	@FindBy (xpath="(//DIV//LABEL)[2]")
	private WebElement bseButton;
	
	@FindBy (xpath="(//DIV//LABEL)[6]")
	private WebElement intradayButton;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[1]")
	private WebElement qtyFiled;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[2]")
	private WebElement priceFiled;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[3]")
	private WebElement triggerPriceFiled;
	
	@FindBy (xpath="//button[@type='submit']")
	private WebElement buyButton;
	
	@FindBy (xpath="//span[@class='user-id']")
	private WebElement userIdButton;
	
	@FindBy (xpath="//a[@href='/logout/']") //div[1]//ul//li[9]//a-->> can also be used
	private WebElement logOutButton;
	
	public KiteZerodhaHomePage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void enterIntoSearch(String name)
	{
		searchButton.sendKeys(name);
	}
	
	public void clickOnShare()
	{
		Actions a = new Actions (driver);
		a.moveToElement(selectShare).perform();
	}
	
	public void clickOnBlueBuyButton()
	{
		selectBlueBuyButton.click();
	}
	
	public void selectBSEButton()
	{
		bseButton.click();
	}
	
	public void selectIntradayButton()
	{
		intradayButton.click();
	}
	
	public void enterQty(String qty)
	{
		qtyFiled.sendKeys(qty);
	}
	
	public void enterprice(String price)
	{
		priceFiled.clear();
		priceFiled.sendKeys(price);
	}
	
	public void enterTriggerPrice(String price)
	{
		triggerPriceFiled.sendKeys(price);
	}
	
	public void ClickOnBuyButton()
	{
		buyButton.click();
	}
	
	public void ClickOnUserIdButton()
	{
		userIdButton.click();
	}
	
	public void ClickOnLogoutButton()
	{
		logOutButton.click();
	}
	
	

}
