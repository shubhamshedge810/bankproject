package POM_Login;

import java.sql.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class KiteZerodhaHomePageOptions 
{
	WebDriver driver1;
	@FindBy (xpath="//a[@href='/dashboard']")
	private WebElement dashboard;

	@FindBy (xpath="//a[@href='/orders']")
	private WebElement orders;
	
	@FindBy (xpath="//a[@href='/positions']")
	private WebElement positions;
	
	@FindBy (xpath="//a[@href='/holdings']")
	private WebElement holdings;
	
	@FindBy (xpath="//a[@href='/funds']")
	private WebElement funds;
	
	@FindBy (xpath="//a[@href='/apps']")
	private WebElement apps;
	
	public KiteZerodhaHomePageOptions (WebDriver driver)
	{
		this.driver1=driver;
		PageFactory.initElements(driver1, this);
	}
	
	public String clickOnDashboard()
	{
		dashboard.click();
		return driver1.getCurrentUrl();
	}
	public String clickOnOrders()
	{
		orders.click();
		return driver1.getCurrentUrl();
	}
	public String clickOnHoldings()
	{
		holdings.click();
		return driver1.getCurrentUrl();
	}
	public String clickOnPositions()
	{
		positions.click();
		return driver1.getCurrentUrl();
	}
	public String clickOnFunds()
	{
		funds.click();
		return driver1.getCurrentUrl();
	}
	public String clickOnApps()
	{
		apps.click();
		return driver1.getCurrentUrl();
	}
}
