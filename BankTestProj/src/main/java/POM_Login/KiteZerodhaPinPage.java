package POM_Login;
//POM CLASS FOR 6 DIGIT PIN VERIFICATION PAGE

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class KiteZerodhaPinPage 
{
	@FindBy (xpath="//input[@id='pin']")
	private WebElement pinInput;
	
	@FindBy (xpath="//button[@type='submit']")
	private WebElement continueButton;
	
	public KiteZerodhaPinPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public void enterPin(String pin)
	{
		pinInput.sendKeys(pin);
	}
	
	public void clickOnContinueButton()
	{
		continueButton.click();
	}
	
}
