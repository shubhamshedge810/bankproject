package POM_Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PracticePOM 
{
	WebDriver driver1;
	@FindBy (xpath="//a[@href='/dashboard']")
	private WebElement dashboard;

	@FindBy (xpath="//a[@href='/orders']")
	private WebElement orders;
	
	@FindBy (xpath="//a[@href='/positions']")
	private WebElement positions;
	
	@FindBy (xpath="//a[@href='/holdings']")
	private WebElement holdings;
	
	@FindBy (xpath="//a[@href='/funds']")
	private WebElement funds;
	
	@FindBy (xpath="//a[@href='/apps']")
	private WebElement apps;
	
	public PracticePOM (WebDriver driver)
	{
		this.driver1=driver;
		PageFactory.initElements(driver1, this);
	}
	
	public String click(String Dashboard)
	{
		dashboard.click();
		return driver1.getCurrentUrl();
	}
	public String click(int Orders)
	{
		orders.click();
		return driver1.getCurrentUrl();
	}
	public String click(boolean Holdings)
	{
		holdings.click();
		return driver1.getCurrentUrl();
	}
	public String click(char Positions)
	{
		positions.click();
		return driver1.getCurrentUrl();
	}
	public String click(float Funds)
	{
		funds.click();
		return driver1.getCurrentUrl();
	}
	public String click(double Apps)
	{
		apps.click();
		return driver1.getCurrentUrl();
	}
}
