package POM_Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class KiteZerodhaLoginPageDetails
{
	@FindBy (xpath="(//div)[8]//h2")
	private WebElement loginToKiteText;
	
	@FindBy (xpath="//div//h2")
	private WebElement kiteImage;
	
	@FindBy (xpath="(//a//img)[2]")
	private WebElement googlePlayStoreImage;
	
	@FindBy (xpath="(//a//img)[3]")
	private WebElement applePlayStoreImage;
	
	@FindBy (xpath="((//a//img)[4])")
	private WebElement zerodhaHomePageLink;
	
	@FindBy (xpath="//a[@class='text-light']")
	private WebElement forNewAccLink;
	
	@FindBy (xpath="//div//p[3]")
	private WebElement zerodhaDetails;
	
	@FindBy (xpath="//div//p[3]//a[1]")
	private WebElement nseLink;
	
	@FindBy (xpath="//div//p[3]//a[2]")
	private WebElement bseLink;
	
	@FindBy (xpath="//div//p[3]//a[3]")
	private WebElement cdslLink1;
	
	@FindBy (xpath="//div//p[3]//a[4]")
	private WebElement cdslLink2;
	
	@FindBy (xpath="//div//p[3]//a[5]")
	private WebElement sebiLink;
	
	@FindBy (xpath="//div//p[3]//a[6]")
	private WebElement mcxLink1;
	
	@FindBy (xpath="//p[@class='text-light text-xxsmall dim']")
	private WebElement appVersionText;
	
	public KiteZerodhaLoginPageDetails(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public boolean checkKiteImages()
	{	
		boolean result= kiteImage.isDisplayed();	
		return result;
		
	}
	public boolean checkGooglePlayStoreImage()
	{
		boolean result = googlePlayStoreImage.isDisplayed();
        return result;  
	}
	public boolean checkApplePlayStoreImage()
	{
		boolean result = applePlayStoreImage.isDisplayed();
		return result;
		
	}

	public String getLoginToKiteText()
	{
		return loginToKiteText.getText();
	}
	
	public void clickOnZerodhaImage()
	{
		zerodhaHomePageLink.click();
	}
	
	public String clickNewAccLink()
	{
		forNewAccLink.click();
		return forNewAccLink.getText();
	}
	
	public boolean getAppVersionText()
	{
		return appVersionText.isDisplayed();
		
	}



}
