package POM_Login;
//POM class for Buy/Cell Window

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class kiteZerodhaBuySellWindow 
{
	@FindBy (xpath="//label[@for='switch-217']")//(//DIV//LABEL)[1]
	private WebElement switchButton;
	
	@FindBy (xpath="(//span[@class='icon icon-info'])[1]")
	private WebElement infoIcon1;
	
	@FindBy (xpath="(//header//div//span[1])[1]")
	private WebElement windowHeadText; //Buy or Sell
	
	@FindBy (xpath="(//header//div//span[1])[2]")
	private WebElement selectedShare;
	
	@FindBy (xpath="(//DIV//LABEL)[2]")
	private WebElement bseButton;

	@FindBy (xpath="(//DIV//LABEL)[3]")
	private WebElement nseButton;
	
	@FindBy (xpath="(//header//div//span[2])[2]")// by default same as selected share for exchange
	private WebElement selectedExchangeName;
	
	@FindBy (xpath="(//header//div//span[3])") //when we select share by defualt it will be 1 and changes as per qty changes
	private WebElement defaultQty;
	
	@FindBy (xpath="(//DIV//LABEL)[4]")
	private WebElement regularOrderButton;

	@FindBy (xpath="(//div//div[2]//label)[4]")
	private WebElement coverOrderButton;
	
	@FindBy (xpath="(//div//div[3]//label)[1]")
	private WebElement bracketOrderButton;

	@FindBy (xpath="(//DIV//LABEL)[5]")
	private WebElement amoButton;
	
	@FindBy (xpath="(//DIV//LABEL)[6]")
	private WebElement intradayButton;
	
	@FindBy (xpath="(//label//span)[3]")
	private WebElement misText;
	
	@FindBy (xpath="(//DIV//LABEL)[7]")
	private WebElement longtermButton;
	
	@FindBy (xpath="(//label//span)[4]")
	private WebElement cncText;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[1]")
	private WebElement qtyFiled;
	
	@FindBy (xpath="(//DIV//LABEL)[8]")
	private WebElement qtyFiledText;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[2]")
	private WebElement priceFiled;
	
	@FindBy (xpath="(//DIV//LABEL)[9]")
	private WebElement priceFiledText;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[3]")
	private WebElement triggerPriceFiled;
	
	@FindBy (xpath="(//DIV//LABEL)[10]")
	private WebElement triggerPriceFiledText;
	
	@FindBy (xpath="(//DIV//LABEL)[11]")
	private WebElement marketRadioButton;
	
	@FindBy (xpath="(//DIV//LABEL)[12]")
	private WebElement limitRadioButton;
	
	@FindBy (xpath="(//DIV//LABEL)[13]")
	private WebElement slRadioButton;
	
	@FindBy (xpath="(//DIV//LABEL)[14]")
	private WebElement slmRadioButton;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[4]")
	private WebElement stoplossFiled;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[5]")
	private WebElement profitFiled;
	
	@FindBy (xpath="(//input[@autocorrect='off'])[6]")
	private WebElement trailingStoplossFiled;
	
	@FindBy (xpath="(//footer//span)[1]")
	private WebElement marginRequiredText;
	
	@FindBy (xpath="(//span[@class='icon icon-info'])[2]")
	private WebElement infoIcon2;
	
	@FindBy (xpath="//a[@data-balloon='Refresh']")
	private WebElement refreshButton;
	
	@FindBy (xpath="//button[@type='submit']")
	private WebElement buyButton;
	
	@FindBy (xpath="//button[@class='button-outline cancel']")
	private WebElement cancleButton;
	
	@FindBy (xpath="//span[@data-balloon='More options']")
	private WebElement moreOptionButton;
	
	@FindBy (xpath="(//label[@class='su-radio-label'])[11]")
	private WebElement dayRadioButton;
	
	@FindBy (xpath="(//label[@class='su-radio-label'])[12]")
	private WebElement immediateOrCancleRadioButton;

	@FindBy (xpath="//span[@data-balloon='Hide options']")
	private WebElement hideOptionButton;	
	
	@FindBy (xpath="//input[@label='Disclosed qty.']")
	private WebElement disclosedQtyFiled;
	
	@FindBy (xpath="(//div//div//label)[18]")
	private WebElement disclosedQtyText;

	@FindBy (xpath="(//div//div//label)[15]")
	private WebElement validityText;

	@FindBy (xpath="(//div//label//span)[5]")
	private WebElement iocText;	
	
	public kiteZerodhaBuySellWindow(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public ArrayList<String> checkTest()
	{
		ArrayList<String> text = new ArrayList<String>();
		text.add(windowHeadText.getText());
		text.add(selectedShare.getText());
		text.add(defaultQty.getText());
		text.add(selectedExchangeName.getText());
		text.add(regularOrderButton.getText());
		text.add(coverOrderButton.getText());
		text.add(bracketOrderButton.getText());
		text.add(amoButton.getText());
		
		return text;
	}
}

