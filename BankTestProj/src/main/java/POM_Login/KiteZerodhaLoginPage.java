package POM_Login;
/* Here We will check login functionality of Kite Zerodha using
 * Valid credentials 
 */

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class KiteZerodhaLoginPage
{
	
	@FindBy (xpath="//input[@id='userid']")
	private WebElement userId;
	
	@FindBy (xpath="//input[@id='password']")
	private WebElement password;
	
	@FindBy (xpath="//button[@type='submit']")
	private WebElement loginButton;
	
	@FindBy (xpath="//a[text()='Forgot password?']")
	private WebElement forgotPasswordLink;
	
	@FindBy (xpath="//a[text()='Change user']")
	private WebElement changeUser;
	
	public KiteZerodhaLoginPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	public void enterUserID(String ID)
	{
		userId.sendKeys(ID);
	}
	
	public void enterPassword(String pwd)
	{
		password.sendKeys(pwd);
	}
	
	public void enterUserID(int ID)
	{
		userId.sendKeys(String.valueOf(ID));
	}
	
	public void enterPassword(int pwd)
	{
		password.sendKeys(String.valueOf(pwd));
	}
	
	public void clickOnLoginButton()
	{
		loginButton.click();
		//loginButton.clear();
	}
	
	public void clickOnForgotPassLink()
	{
		forgotPasswordLink.click();
	}
	
	public void clickOnChangeUser()
	{
		changeUser.click();
	}
	public void clearfield()
	{
		userId.clear();
		password.clear();
	}
	
	
}
