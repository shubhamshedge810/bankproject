package Utilities;

import java.io.File;
import java.io.IOException;

//import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

public class Utility 
{	
	public static void takeScreenshot(WebDriver driver) throws IOException
	{
		File source = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		File dest = new File("test-output"+File.separator+"takeScreenshot"+File.separator+"image.jpg");
		FileHandler.copy(source, dest); //From selenium library
		//FileUtils.copyFile(source, dest); //from apache library
	}

}
